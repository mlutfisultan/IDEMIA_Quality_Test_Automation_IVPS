package Enhanced_pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class C3_Form_125_Page {

    public static String ViolationCode;
    public By Form_125_Generator_Btn = By.id("pages-entry-form125-generator");
    public By Radar_Code_Btn = By.id("violations-filter-parameters-radarcode-input");
    public By Arrangement_Type_Droplst = By.id("violations-filter-parameters-sort-type-input");
    public By Violation_From_Date_Input = By.cssSelector("input.ng-tns-c89-6.p-inputtext.p-component.ng-star-inserted");
    public By Violation_To_Date_Input = By.cssSelector("input.ng-tns-c89-7.p-inputtext.p-component.ng-star-inserted");
    public By From_Data_Month_DropList = By.cssSelector("select.p-datepicker-month.ng-tns-c89-6.ng-star-inserted");
    public By From_Data_Day = By.xpath(("(//span[text()='1'])[1]"));
    public By To_Data_Month_DropList = By.cssSelector("select.p-datepicker-month.ng-tns-c89-7.ng-star-inserted");
    public By Review_Activation = By.id("violations-filter-parameters-review-form-input");
    public By View_Violations_Btn = By.id("violations-filter-parameters-show-violations-button");
    public By Cancel_Btn = By.id("violations-filter-parameters-cancel-button");
    public By Old_Plate_ID_CBox = By.id("form125-generator-oldPlateNumber-input");
    public By Old_Plate_ID_Cities_List = By.id("form125-generator-governorates-input");
    public By Old_Plate_ID_Cities = By.cssSelector("li.p-dropdown-item.p-ripple");
    public By Plate_Source_Locator = By.cssSelector("div.move");
    public By Plate_Destination_Locator = By.cssSelector("img.source-image.ng-star-inserted");
    public By Plate_ID_1 = By.id("cpn1");
    public By Plate_ID_2 = By.id("cpn2");
    public By Plate_ID_3 = By.id("cpn3");
    public By Plate_ID_4 = By.id("cpn4");
    public By Plate_ID_5 = By.id("cpn5");
    public By Type_of_Vio_Offense_List = By.cssSelector("#form125-generator-select-violations-input .p-multiselect-trigger");// Drop Menu
    public By Type_of_Vio_Offense_Childs = By.cssSelector("li.p-multiselect-item.p-ripple");
    public By Offenses_List_closer_Btn = By.cssSelector(".p-multiselect-close-icon");
    public By Form_Creation_Btn = By.id("form125-generator-submit-form125-button");
    public By Violation_Code_Label= By.xpath("//td[@style='word-break: break-all;']");
    public By Form_Creation_ConfirmationBtn = By.id("form125-generateForm125-button");

    public By Form125_ConfirmationMessage = By.xpath("//div[@aria-label= 'تم بنجاح']");
    public By Skip_Violation_Btn = By.id("form125-generator-skip-violation-button");
    public By Confirmation_Yes_Btn = By.id("form125-generator-dialog-yes");
    public By Confirmation_No_Btn = By.id("form125-generator-dialog-no");
    public By Full_View_Image_Btn = By.id("form125-generator-view-full-image-button");
    public By Vehicle_Image_Processing_Btn = By.id("image-cropper-view-cropped-image-button");
    public By Form_125_Generation_Btn = By.id("form125-generateForm125-button");
    public By Back_Btn = By.id("breadcrumb-back-button");
    public By IDEMIA_Spinner = By.cssSelector("i.fa-solid.fa-spinner.label-color.fa-spin.loader-size");

    // <div role="alertdialog" aria-live="polite" class="ng-tns-c38-52 toast-message ng-star-inserted" aria-label="تم بنجاح" style> تم بنجاح </div>
    WebDriver driver;

    public C3_Form_125_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Wait_Invisibility_Of_Element(By Element) {
        WebDriverWait Wait = new WebDriverWait(driver, 60);
        Wait.until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(Element)));
    }

    public void Type_Form_125_Plate_ID() {
        ElementActions.type(driver, Plate_ID_1, O15_Plate_Number_Generator.getPlateNumberByIndex(0));
        ElementActions.type(driver, Plate_ID_2, O15_Plate_Number_Generator.getPlateNumberByIndex(1));
        ElementActions.type(driver, Plate_ID_3, O15_Plate_Number_Generator.getPlateNumberByIndex(2));
//        ElementActions.type(driver, Form_125_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, Plate_ID_5, O15_Plate_Number_Generator.getPlateNumberByIndex(3));
    }
    public void Select_Form_125_Type_Offense_Childs() {
        List<WebElement>  Offense_Childs  = driver.findElements(Type_of_Vio_Offense_Childs);
       for (int i = 0 ; i < Offense_Childs.size() && i!= 8   ; i++)
       {
           Offense_Childs.get(i).click();
       }
    }

    public void Select_Offenses_List_closer_Btn() {
        ElementActions.click(driver, Offenses_List_closer_Btn);
    }

    public void Select_Form_125_Generation_Btn() {
        ElementActions.click(driver, Form_125_Generation_Btn);
    }

    public void Fill_Input_125_Form(String ArrangementType , String ReviewActivation , String FromMonth )
    {
        ElementActions.click(driver, Form_125_Generator_Btn);
        ElementActions.click(driver, Arrangement_Type_Droplst);
        SelectDroplst(ArrangementType , Arrangement_Type_Droplst);
        ElementActions.click(driver, Violation_From_Date_Input);
        SelectDroplst(FromMonth , From_Data_Month_DropList);
        ElementActions.click(driver ,From_Data_Day);
        ElementActions.click(driver, Review_Activation);
        SelectDroplst(ReviewActivation , Review_Activation);
        ElementActions.click(driver, View_Violations_Btn);
    }
    public void Create_and_Submit_125_Form(String PlateType , String ReviewActivation )
    {
        if (PlateType == "Old") {
            ElementActions.click(driver, Old_Plate_ID_CBox);
            ElementActions.click(driver, Old_Plate_ID_Cities_List);
            List<WebElement> Cities = driver.findElements(Old_Plate_ID_Cities);
            Cities.get(13).click();
            ElementActions.type(driver, Plate_ID_5, O15_Plate_Number_Generator.getOldPlateNumberByIndex(0));
        }
        else if ((PlateType == "New"))
        {
            Type_Form_125_Plate_ID();
        }
        ElementActions.click(driver, Type_of_Vio_Offense_List);
        Select_Form_125_Type_Offense_Childs();
        ElementActions.click(driver, Form_Creation_Btn);
        if (ReviewActivation == "true" )
        {
            ViolationCode = driver.findElement(Violation_Code_Label).getText();
            //Wait_Invisibility_Of_Element(Form_Creation_ConfirmationBtn);
            ElementActions.click(driver , Form_Creation_ConfirmationBtn);
        }

    }
    public void SelectDroplst(String Value , By Element)
    {
        Select DropLst = new Select(driver.findElement(Element));
        DropLst.selectByValue(Value);
    }
}
