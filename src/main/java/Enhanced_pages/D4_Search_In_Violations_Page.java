package Enhanced_pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class D4_Search_In_Violations_Page {
    public By Search_Violations_Page_Btn = By.id("pages-entry-violations-manager");
    public By Search_Violations_Label = By.cssSelector("#breadcrumb-violations-manager > span");
    public By Search_By_Plate_Number = By.id("form-control-plateNumber-input");
    public By Search_By_Code = By.id("form-control-violationCode-input");
    public By Search_By_Username = By.id("form-control-userName-input");
    public By Search_By_Status_List = By.cssSelector("#form-control-status-input .p-multiselect-label-container");

    public By Violation_Statuses= By.cssSelector("li.p-multiselect-item.p-ripple");
    public By Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Search_Result = By.cssSelector("td.plate-number-col.plate-font");
    public By Total_Search_Result = By.xpath("//tfoot/tr/td[2]");
    public By Return_Vio_Btn = By.id("violations-manager-reset-violation-button-0");
    public By Edit_Vio_Btn = By.id("violations-manager-regenerate-violation-button-0");
    public By Vio_Data_Btn = By.id("violations-manager-viewForm-button-0");
    public By Vio_Date_Btn = By.id("violations-manager-history-violation-button-0");
    public By Closure_Btn = By.cssSelector(".p-dialog-header-close-icon");

    WebDriver driver;

    public D4_Search_In_Violations_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Search_Violations_Btn() {
        ElementActions.click(driver, Search_Violations_Page_Btn);
    }

    public void Type_Search_For_Violations_Code(String ViolationCode) {
        ElementActions.type(driver ,Search_By_Plate_Number , "" );
        ElementActions.type(driver, Search_By_Username, "");
        ElementActions.type(driver, Search_By_Code, ViolationCode);
        ElementActions.click(driver, Search_Btn);
    }

    public void Type_Search_For_Violations_By_Username(String Username)
    {
        ElementActions.type(driver ,Search_By_Plate_Number , "" );
        ElementActions.type(driver ,Search_By_Code , "" );
        ElementActions.type(driver, Search_By_Username, Username);
        ElementActions.click(driver, Search_Btn);

    }

    public void Select_Search_For_Violations_By_Status()
    {
        ElementActions.type(driver ,Search_By_Plate_Number , "" );
        ElementActions.type(driver ,Search_By_Code , "" );
        ElementActions.type(driver ,Search_By_Username , "" );
        ElementActions.click(driver, Search_By_Status_List);
        List<WebElement> Violation_Status  = driver.findElements(Violation_Statuses);
        Violation_Status.get(0).click();
        ElementActions.click(driver, Search_Btn);

    }
    public void Search_For_Violations_With_Plate(String PlateType)
    {
        if (PlateType == "New")
        {
            ElementActions.type(driver, Search_By_Plate_Number,O15_Plate_Number_Generator.getNewPlateNumber());
        }
        else
        {
            ElementActions.type(driver, Search_By_Plate_Number, O15_Plate_Number_Generator.getOldPlateNumber());
        }
            ElementActions.click(driver, Search_Btn);


    }
}
