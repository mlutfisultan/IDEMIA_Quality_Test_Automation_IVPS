package Enhanced_pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.shaft.gui.element.ElementActions.click;


public class B2_Welcome_Page {
    public By Welcome_Page_Label = By.cssSelector("#breadcrumb-main-menu > span");
    public By Main_Menu_Btn = By.id("header-mainmenu-link");
    public By Violation_Management_Page_Btn = By.id("pages-entry-violations-management");
    public By Violations_Department_Label = By.cssSelector("#breadcrumb-violations-management > span");
    public By White_List_Cars_Page_Btn = By.id("pages-entry-whitelist-manager");
    public By Black_List_Cars_Page_Btn = By.id("pages-entry-blacklist-manager");
    public By Radars_Page_Btn = By.id("pages-entry-mesta-manager");
    public By System_User_Management_Page_Btn = By.id("pages-entry-users-management");
    public By Reports_FollowUp_Page_Btn = By.id("pages-entry-reports-dashboard");
    public By Statistics_Page_Btn = By.id("pages-entry-analytics");
    public By Monitor_Panel_Page_Btn = By.id("header-watchboard-link");
    public By System_Settings_Btn = By.id("header-appconfig-link");
    public By Account_Label = By.cssSelector("#header-user-button .p-button-label");
    public By Logout_Menu = By.cssSelector("#header-user-button .p-splitbutton-menubutton");
    public By Change_Password_Btn = By.id("header-user-dropdown-change-password-button");
    public By Logout_Btn = By.id("header-user-dropdown-logout-button");
    WebDriver driver;

    public B2_Welcome_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Main_Menu_Btn() {
        ElementActions.click(driver, Main_Menu_Btn);
    }

    public void Select_Violation_Management_Btn() {
        ElementActions.click(driver, Violation_Management_Page_Btn);
    }


    public void Select_Black_List_Cars_Btn()
    {
        ElementActions.click(driver, Black_List_Cars_Page_Btn);
    }
    public void IVPS_Logout() {
        click(driver, Logout_Menu);
        click(driver, Logout_Btn);
    }
}
