package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class C3_Form_125_Page {
    public By Form_125_Generator_Btn = By.id("pages-entry-form125-generator");
    public By Radar_Code_Btn = By.id("violations-filter-parameters-radarcode-input");
    public By Arrangement_Type = By.id("violations-filter-parameters-sort-type-input");// Drop Menu
    public By Arrange_Type_ASC = By.id("violations-filter-parameters-sort-type-input-option-asc");
    public By Arrange_Type_DSC = By.id("violations-filter-parameters-sort-type-input-option-desc");
    public By Violation_From_Date_Input = By.cssSelector("#violations-filter-parameters-violation-date-input .p-inputtext");
    public By Violation_To_Date_Input = By.cssSelector("input.ng-tns-c89-15.p-inputtext.p-component.ng-star-inserted");

    public By Review_Activation = By.id("violations-filter-parameters-review-form-input");
    public By Review_Activation_Yes = By.id("violations-filter-parameters-review-form-input-option-yes");
    public By Review_Activation_No = By.id("violations-filter-parameters-review-form-input-option-no");
    public By View_Violations_Btn = By.id("violations-filter-parameters-show-violations-button");
    public By Cancel_Btn = By.id("violations-filter-parameters-cancel-button");
    public By Old_Plate_ID_CBox = By.id("form125-generator-oldPlateNumber-input");
    public By Old_Plate_ID_Cities_List = By.id("form125-generator-governorates-input");
    public By Old_Plate_ID_Selected_City = By.xpath("//span[text()='القاهـــرة']");
    public By Plate_Source_Locator = By.cssSelector("div.move");
    public By Plate_Destination_Locator = By.cssSelector("img.source-image.ng-star-inserted");
    public By Plate_ID_1 = By.id("cpn1");
    public By Plate_ID_2 = By.id("cpn2");
    public By Plate_ID_3 = By.id("cpn3");
    public By Plate_ID_4 = By.id("cpn4");
    public By Plate_ID_5 = By.id("cpn5");
    public By Type_of_Vio_Offense_List = By.cssSelector("#form125-generator-select-violations-input .p-multiselect-trigger");// Drop Menu
    public By Type_of_Vio_Offense_Child1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child5 = By.cssSelector("p-multiselectitem:nth-child(5)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child6 = By.cssSelector("p-multiselectitem:nth-child(6)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child7 = By.cssSelector("p-multiselectitem:nth-child(7)  > .p-multiselect-item");
    public By Type_of_Vio_Offense_Child8 = By.cssSelector("p-multiselectitem:nth-child(8)  > .p-multiselect-item");
    public By Offenses_List_closer_Btn = By.cssSelector(".p-multiselect-close-icon");
    public By Form_Creation_Btn = By.id("form125-generator-submit-form125-button");
    public By Skip_Violation_Btn = By.id("form125-generator-skip-violation-button");
    public By Skip_Confirmation_Yes_Btn = By.id("form125-generator-dialog-yes");
    public By Skip_Confirmation_No_Btn = By.id("form125-generator-dialog-no");
    public By Full_View_Image_Btn = By.id("form125-generator-view-full-image-button");
    public By Vehicle_Image_Processing_Btn = By.id("image-cropper-view-cropped-image-button");
    public By Form_125_Generation_Btn = By.id("form125-generateForm125-button");
    public By Back_Btn = By.id("breadcrumb-back-button");
    public By IDEMIA_Spinner = By.cssSelector("i.fa-solid.fa-spinner.label-color.fa-spin.loader-size");

    WebDriver driver;
//    String Plate_ID_1
//    String Plate_ID_2
//    String Plate_ID_3
//    String Plate_ID_4
//    String Plate_ID_5

    public C3_Form_125_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Wait_Invisibility_Of_Page_Spinner() {
        WebDriverWait Wait = new WebDriverWait(driver, 60);
        Wait.until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(IDEMIA_Spinner)));
    }

    public void Select_Form_125_Generator_Btn() {
        ElementActions.click(driver, Form_125_Generator_Btn);
    }

    public void Select_Form_125_Radar_Code_Btn() {
        ElementActions.click(driver, Radar_Code_Btn);
    }

    public void Select_Form_125_Arrangement_Type() {
        ElementActions.click(driver, Arrangement_Type);
    }

    public void Select_Form_125_Arrangement_Type_Asc() {
        ElementActions.click(driver, Arrange_Type_ASC);
    }

    public void Select_Form_125_Arrangement_Type_Dsc() {
        ElementActions.click(driver, Arrange_Type_DSC);
    }

    public void Type_Form_125_Violation_From_Date() {

        ElementActions.type(driver, Violation_From_Date_Input, "2015/01/01");
    }
    public void Type_Form_125_Violation_To_Date() {

        ElementActions.type(driver, Violation_To_Date_Input, "2015/01/01");
    }
    public void Select_Form_125_Review_Activation() {
        ElementActions.click(driver, Review_Activation);
    }

    public void Select_Form_125_Review_Activation_Yes() {
        ElementActions.click(driver, Review_Activation_Yes);
    }

    public void Select_Form_125_Review_Activation_No() {
        ElementActions.click(driver, Review_Activation_No);
    }

    public void Select_Form_125_View_Violations_Btn() {
        ElementActions.click(driver, View_Violations_Btn);
    }

    public void Select_Form_125_Cancel_Btn() {
        ElementActions.click(driver, Cancel_Btn);
    }

    public void Select_Form_125_old_Plate_ID_CBox() {
        ElementActions.click(driver, Old_Plate_ID_CBox);
    }

    public void Select_Form_125_Cities_Menu() {
        ElementActions.click(driver, Old_Plate_ID_Cities_List);
    }

    public void Search_Form_125_old_Paintings_City() {
        ElementActions.click(driver, Old_Plate_ID_Selected_City);
    }

    public void Drag_Plate_Identifier() {
        ElementActions.dragAndDrop(driver, Plate_Source_Locator, Plate_Destination_Locator);
    }
    public void Type_Form_125_Plate_ID_5() {

        ElementActions.type(driver, Plate_ID_5, Enhanced_pages.O15_Plate_Number_Generator.getOldPlateNumberByIndex(0));
    }

    public void Type_Form_125_Plate_ID() {
        ElementActions.type(driver, Plate_ID_1, pages.O15_Plate_Number_Generator.getPlateNumberByIndex(0));
        ElementActions.type(driver, Plate_ID_2, pages.O15_Plate_Number_Generator.getPlateNumberByIndex(1));
        ElementActions.type(driver, Plate_ID_3, pages.O15_Plate_Number_Generator.getPlateNumberByIndex(2));
//        ElementActions.type(driver, Form_125_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, Plate_ID_5, pages.O15_Plate_Number_Generator.getPlateNumberByIndex(3));
    }

    public void Select_Form_125_Type_of_Offense() {
        ElementActions.click(driver, Type_of_Vio_Offense_List);
    }

    public void Select_Form_125_Type_Offense_Child1() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child1);
    }

    public void Select_Form_125_Type_Offense_Child2() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child2);
    }

    public void Select_Form_125_Type_Offense_Child3() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child3);
    }

    public void Select_Form_125_Type_Offense_Child5() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child5);
    }

    public void Select_Form_125_Type_Offense_Child6() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child6);
    }

    public void Select_Form_125_Type_Offense_Child7() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child7);
    }

    public void Select_Form_125_Type_Offense_Child8() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child8);
    }

    public void Select_List_Of_Form_125_Type_Offense_Child() {
        ElementActions.click(driver, Type_of_Vio_Offense_Child1);
    }

    public void Select_Offenses_List_closer_Btn() {
        ElementActions.click(driver, Offenses_List_closer_Btn);
    }

    public void Select_Form_125_Creation_Btn() {
        ElementActions.click(driver, Form_Creation_Btn);
    }

    public void Select_Form_125_Full_View_Image_Btn() {
        ElementActions.click(driver, Full_View_Image_Btn);
    }

    public void Select_Form_125_Skip_Violation_Btn() {
        ElementActions.click(driver, Skip_Violation_Btn);
    }

    public void Select_Form_125_Skip_Confirmation_Yes_Btn() {
        ElementActions.click(driver, Skip_Confirmation_Yes_Btn);
    }

    public void Select_Form_125_Skip_Confirmation_No_Btn() {
        ElementActions.click(driver, Skip_Confirmation_No_Btn);
    }

    public void Select_Form_125_Vehicle_Image_Processing_Btn() {
        ElementActions.click(driver, Vehicle_Image_Processing_Btn);
    }

    public void Select_Form_125_Generation_Btn() {
        ElementActions.click(driver, Form_125_Generation_Btn);
    }

    public void Select_Form_125_Back_Btn() {
        ElementActions.click(driver, Back_Btn);
    }

}
