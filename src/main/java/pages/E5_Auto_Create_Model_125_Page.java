package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class E5_Auto_Create_Model_125_Page {
    public By Auto_Create_125_Model_Page_Btn = By.id("pages-entry-automatic-generation");
    public By Create_Forms_Automatically_Btn = By.id("ivps-tool-bar-form125batchgenerator-addnew-0");
    public By Date_From = By.cssSelector("#add-form125-batch-generator-datefrom-input .p-inputtext");
    public By Date_To = By.cssSelector("#add-form125-batch-generator-dateto-input .p-inputtext");
    public By Radar_List = By.cssSelector("#add-form125-batch-generator-radar-input .p-inputtext");
    public By Selected_Radar_child_EGY004 = By.xpath("//span[text()='EGY004']");
    public By Selected_Radar_child_EGY005 = By.xpath("//span[text()='EGY005']");
    public By Search_Of_Models_Count = By.cssSelector(".p-3:nth-child(4) > .ng-star-inserted");
    public By Generation_Form_Confirmation_Yes_Btn = By.id("add-form125-batch-generator-confirm-button");
    public By Generation_Form_Confirmation_No_Btn = By.id("add-form125-batch-generator-cancel-button");
    public By Current_Generated_Forms_Btn = By.id("ivps-tool-bar-form125batchgenerator-current-1");
    public By Old_Generated_Forms_Btn = By.id("ivps-tool-bar-form125batchgenerator-archived-2");
    public By Total_of_Forms_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Back_Btn = By.id("breadcrumb-back-button");
    WebDriver driver;

    public E5_Auto_Create_Model_125_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Auto_Create_125_Model_Page_Btn() {
        ElementActions.click(driver, Auto_Create_125_Model_Page_Btn);
    }

    public void Select_Auto_Create_125_Model_Create_Forms_Automatically_Btn() {
        ElementActions.click(driver, Create_Forms_Automatically_Btn);
    }

    public void Type_Auto_Create_125_From_Date() {

        ElementActions.type(driver, Date_From, "2022/02/01");
    }

    public void Type_Auto_Create_125_To_Date() {

        ElementActions.type(driver, Date_To, "2022/02/28");
    }

    public void Select_Auto_Create_125_Radar_List() {
        ElementActions.click(driver, Radar_List);
    }

    public void Select_Auto_Create_125_Radar_List_child_EGY004() {
        ElementActions.click(driver, Selected_Radar_child_EGY004);
    }

    public void Get_Search_Of_Models_Count() {
        ElementActions.getElementsCount(driver, Search_Of_Models_Count);
    }

    public void Select_Auto_Create_125_Form_Yes_Btn() {
        ElementActions.click(driver, Generation_Form_Confirmation_Yes_Btn);
    }

    public void Select_Auto_Create_125_Form_No_Btn() {
        ElementActions.click(driver, Generation_Form_Confirmation_No_Btn);
    }

    public void Select_Auto_Create_125_Model_Current_Forms_Btn() {
        ElementActions.click(driver, Current_Generated_Forms_Btn);
    }

    public void Select_Auto_Create_125_Model_Old_Forms_Btn() {
        ElementActions.click(driver, Old_Generated_Forms_Btn);
    }

    public void Get_Auto_Create_125_Model_Total_Forms_Count() {
        ElementActions.getElementsCount(driver, Total_of_Forms_Count);
    }

    public void Select_Auto_Create_125_Form_Back_Btn() {
        ElementActions.click(driver, Back_Btn);
    }
}
