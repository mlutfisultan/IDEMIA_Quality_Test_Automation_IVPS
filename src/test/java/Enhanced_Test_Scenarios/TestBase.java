package Enhanced_Test_Scenarios;

import Enhanced_pages.B2_Welcome_Page;
import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class TestBase
{
    protected  WebDriver driver;
    protected B2_Welcome_Page WelcomePage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");

    }

    @AfterClass
    public void tearDown() {
        /*WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);*/
    }
}
