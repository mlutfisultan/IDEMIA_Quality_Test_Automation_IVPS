package Enhanced_Test_Scenarios;

import Enhanced_pages.D4_Search_In_Violations_Page;
import Enhanced_pages.O15_Plate_Number_Generator;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.Test;
import Enhanced_pages.A1_Main_Page;
import Enhanced_pages.B2_Welcome_Page;

public class C3_Search_for_Violation_By_New_Plate_ID_TestEnhnaced extends TestBase {
    private String PlateType = "New";
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private D4_Search_In_Violations_Page SearchInViolationsPage_Obj;

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj = new Enhanced_pages.A1_Main_Page(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(Welcome_Page_Label.contains("القائمة الرئيسية"));
    }

    @Test(description = "Search of Violations for New Plate ID", priority = 3)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_With_New_Plate() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Violations_Department_Label).getText().contains("ادارة المخالفات" ));
        SearchInViolationsPage_Obj = new D4_Search_In_Violations_Page(driver);
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        Assert.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Search_Violations_Label).getText().contains("البحث فى المخالفات" ));
        SearchInViolationsPage_Obj.Search_For_Violations_With_Plate(PlateType);
        //Assert.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Search_Result).getText() == driver.findElement(SearchInViolationsPage_Obj.Search_By_Plate_Number).getText());
        String Search_Result = driver.findElement(SearchInViolationsPage_Obj.Total_Search_Result).getText();
        Assert.assertTrue(Search_Result.contains("1"));
    }

    @Test(description = "Search of Violations By Status", priority = 4)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_By_Status()
    {
        SearchInViolationsPage_Obj.Select_Search_For_Violations_By_Status();
    }

}
