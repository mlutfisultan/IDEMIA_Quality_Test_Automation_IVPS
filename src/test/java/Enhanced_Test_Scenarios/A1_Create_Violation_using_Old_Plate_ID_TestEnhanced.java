package Enhanced_Test_Scenarios;

import Enhanced_pages.A1_Main_Page;
import Enhanced_pages.B2_Welcome_Page;
import Enhanced_pages.C3_Form_125_Page;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.Test;


public class A1_Create_Violation_using_Old_Plate_ID_TestEnhanced extends TestBase {

    private String ArrangementType = "DESC";
    private String FromMonth = "0";
    private String ReviewActivation = "true";
    private String PlateType = "Old";
    private A1_Main_Page MainPage_Obj;
    private C3_Form_125_Page Form125Page_Obj;

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj = new Enhanced_pages.A1_Main_Page(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(Welcome_Page_Label.contains("القائمة الرئيسية" ));
    }

    @Test(description = "Create and Submit 125 Form for Old Plate ID", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_and_Submit_125_Form() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Violations_Department_Label).getText().contains("ادارة المخالفات" ));
        Form125Page_Obj = new Enhanced_pages.C3_Form_125_Page(driver);
        Form125Page_Obj.Fill_Input_125_Form(ArrangementType , ReviewActivation , FromMonth );
        Form125Page_Obj.Create_and_Submit_125_Form(PlateType , ReviewActivation );
        /*driver.manage().timeouts().implicitlyWait(2000 , TimeUnit.MILLISECONDS);
        Assert.assertTrue(driver.findElement(Form125Page_Obj.Form125_ConfirmationMessage).isDisplayed());*/
    }
}
