package Enhanced_Test_Scenarios;


import Enhanced_pages.A1_Main_Page;
import Enhanced_pages.B2_Welcome_Page;
import Enhanced_pages.C3_Form_125_Page;
import Enhanced_pages.D4_Search_In_Violations_Page;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.Test;


public class D4_Search_for_Violation_By_Old_Plate_ID_TestEnhanced extends TestBase {
    private String PlateType = "Old";
    private String username= "IVPS Test Account";
    private  String Search_Result;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private D4_Search_In_Violations_Page SearchInViolationsPage_Obj;

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj = new Enhanced_pages.A1_Main_Page(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new Enhanced_pages.B2_Welcome_Page(driver);
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(Welcome_Page_Label.contains("القائمة الرئيسية"));
    }

    @Test(description = "Search of Violations for Old Plate ID", priority = 5)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_With_Old_Plate() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Assert.assertTrue(driver.findElement(WelcomePage_Obj.Violations_Department_Label).getText().contains("ادارة المخالفات" ));
        SearchInViolationsPage_Obj = new D4_Search_In_Violations_Page(driver);
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        Assert.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Search_Violations_Label).getText().contains("البحث فى المخالفات" ));
        SearchInViolationsPage_Obj.Search_For_Violations_With_Plate(PlateType);
        //Assert.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Search_Result).getText() == driver.findElement(SearchInViolationsPage_Obj.Search_By_Plate_Number).getText());
        Search_Result = driver.findElement(SearchInViolationsPage_Obj.Total_Search_Result).getText();
        Assert.assertTrue(Search_Result.contains("1"));
    }

    @Test(description = "Search of Violations By Code", priority = 6)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_By_Code()
    {
        SearchInViolationsPage_Obj.Type_Search_For_Violations_Code(C3_Form_125_Page.ViolationCode);
        Search_Result = driver.findElement(SearchInViolationsPage_Obj.Total_Search_Result).getText();
        Assert.assertTrue(Search_Result.contains("1"));
    }

    @Test(description = "Search of Violations By Username", priority = 7)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_By_Username()
    {
        SearchInViolationsPage_Obj.Type_Search_For_Violations_By_Username(username);
         Search_Result = driver.findElement(SearchInViolationsPage_Obj.Total_Search_Result).getText();
        Assert.assertTrue(Search_Result.contains("1"));
    }
}
