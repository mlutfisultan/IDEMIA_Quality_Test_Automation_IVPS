package Enhanced_Test_Scenarios;

import Enhanced_pages.A1_Main_Page;
import Enhanced_pages.B2_Welcome_Page;
import Enhanced_pages.H8_Black_List_Cars_Page;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.Test;

public class E5_Black_List_Search_and_Add_TestEnhanced extends TestBase{
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private H8_Black_List_Cars_Page BlackListPage_Obj;
    private String Platetype = "New" ;

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj = new Enhanced_pages.A1_Main_Page(driver);
        MainPage_Obj.IVPS_SignIn();
        WelcomePage_Obj = new Enhanced_pages.B2_Welcome_Page(driver);
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        Assert.assertTrue(Welcome_Page_Label.contains("القائمة الرئيسية" ));
    }

    @Test(description = "Search for Added Plate ID in Black List Previously", priority = 1)
    @Severity(SeverityLevel.MINOR)
    public void Search_for_Black_List_Car_Added_Before() {
        WelcomePage_Obj.Select_Black_List_Cars_Btn();
        BlackListPage_Obj = new H8_Black_List_Cars_Page(driver);
        Assert.assertTrue(driver.findElement(BlackListPage_Obj.Black_List_Cars_Lable).getText().contains("قائمة السيارات المحظورة"));
        BlackListPage_Obj.Type_Black_List_Cars_Search_using_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Search_Plate_ID_Btn();
        BlackListPage_Obj.Get_Black_List_Cars_Search_Plate_Count();
        String Get_Black_List_Cars_Search_Plate_Count = driver.findElement(BlackListPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Black_List_Cars_Search_Plate_Count);
        BlackListPage_Obj.Select_Black_List_Cars_DisplayAll_Plate_ID_Btn();
    }

    @Test(description = "Add Plate ID Black List", priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void Add_Car_in_Black_List() {
        WelcomePage_Obj.Select_Black_List_Cars_Btn();
        BlackListPage_Obj = new H8_Black_List_Cars_Page(driver);
        Assert.assertTrue(driver.findElement(BlackListPage_Obj.Black_List_Cars_Lable).getText().contains("قائمة السيارات المحظورة"));
        /*BlackListPage_Obj.Select_Black_List_Cars_New_Plate_Number_Btn();
        BlackListPage_Obj.Type_Black_List_Cars_Add_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Add_Plate_ID_Btn();*/
        for (int i=0 ; i <2 ; i++) {
            BlackListPage_Obj.Add_Car_in_Black_List(Platetype);
            Platetype = "Old";
        }
        String Get_White_List_Success_Msg = driver.findElement(BlackListPage_Obj.Success_Msg).getText();
        System.out.println("Search result = " + Get_White_List_Success_Msg);
    }

    @Test(description = "Search for Added Plate ID in Black List Recently ", priority = 3)
    @Severity(SeverityLevel.NORMAL)
    public void Search_FOR_Add_BlackListCar() {
        BlackListPage_Obj.Type_Black_List_Cars_Search_using_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Search_Plate_ID_Btn();
        BlackListPage_Obj.Get_Black_List_Cars_Search_Plate_Count();
        String Get_Black_List_Cars_Search_Plate_Count = driver.findElement(BlackListPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Black_List_Cars_Search_Plate_Count);
        BlackListPage_Obj.Select_Black_List_Cars_DisplayAll_Plate_ID_Btn();
    }
}
