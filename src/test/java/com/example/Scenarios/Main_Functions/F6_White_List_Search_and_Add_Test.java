package com.example.Scenarios.Main_Functions;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.G7_White_List_Cars_Page;

public class F6_White_List_Search_and_Add_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private G7_White_List_Cars_Page WhiteListPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        WhiteListPage_Obj = new G7_White_List_Cars_Page(driver);

    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Search for Added Plate ID in White List Previously", priority = 1)
    @Severity(SeverityLevel.MINOR)
    public void Search_for_White_List_Car_Added_Before() {
        WelcomePage_Obj.Select_White_List_Cars_Btn();
        WhiteListPage_Obj.Type_White_List_Search_using_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Search_Plate_ID_Btn();
        WhiteListPage_Obj.Get_White_List_Search_Plate_Count();
        String Get_White_List_Search_Plate_Count = driver.findElement(WhiteListPage_Obj.Search_Plate_Count).getText();
        System.out.println("Search result = " + Get_White_List_Search_Plate_Count);
        WhiteListPage_Obj.Select_White_List_DisplayAll_Plate_ID_Btn();
    }

    @Test(description = "Add Plate ID White List", priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void Add_Car_in_White_List() {
        WhiteListPage_Obj.Select_White_List_New_Plate_Number_Btn();
        WhiteListPage_Obj.Type_White_List_Add_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Add_Plate_ID_Btn();
        String Get_White_List_Success_Msg = driver.findElement(WhiteListPage_Obj.Success_Msg).getText();
        System.out.println("Search result = " + Get_White_List_Success_Msg);
    }

    @Test(description = "Search for Added Plate ID in White List Recently", priority = 3)
    @Severity(SeverityLevel.NORMAL)
    public void Search_FOR_Added_New_Plate_ID_in_White_List() {
        WhiteListPage_Obj.Type_White_List_Search_using_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Search_Plate_ID_Btn();
        WhiteListPage_Obj.Get_White_List_Search_Plate_Count();
        String Get_White_List_Search_Plate_Count = driver.findElement(WhiteListPage_Obj.Search_Plate_Count).getText();
        System.out.println("Search result = " + Get_White_List_Search_Plate_Count);
        WhiteListPage_Obj.Select_White_List_DisplayAll_Plate_ID_Btn();
    }


}
